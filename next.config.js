const nextConfig = {
	output: 'export',
	reactStrictMode: true,
	assetPrefix:
		process.env.NODE_ENV === 'production' ? '/cmaffiliates-test' : '',

	env: {
		GEOCODING_API_URL: process.env.GEOCODING_API_URL,
		WEATHER_API_URL: process.env.WEATHER_API_URL,
	},
};

module.exports = nextConfig;
