const Header = () => {
	return (
		<header className="border-b-2 border-gray-light">
			<div className="container mx-auto flex py-4 justify-between">
				{/*<Link className="text-2xl text-gray-300" href={RootPath.Index()}>*/}
				{/*	Weather*/}
				{/*</Link>				*/}
				<h2 className="text-2xl text-gray-300">Weather</h2>
			</div>
		</header>
	);
};

export default Header;
